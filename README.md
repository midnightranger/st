This is my personal build of 'st' Terminal emulator from suckless.org Team. The patches I have applied are the below:

1. alpha: This patch allows users to change the opacity of the background. Note that you need an X composite manager (e.g. compton, xcompmgr) to make this patch effective.
2. scrollback: Allows scroll back through terminal output using Shift+{PageUp, PageDown}.
3. xresources: This patch adds the ability to configure st via Xresources. At startup, st will read and apply the resources named in the resources[] array in config.h.
4. blinking cursor: This patch allows the use of a blinking cursor.
5. delkey: Return BS on pressing backspace and DEL on pressing the delete key.
6. dynamic-cursor-color: Swaps the colors of your cursor and the character you're currently on (much like alacritty).
